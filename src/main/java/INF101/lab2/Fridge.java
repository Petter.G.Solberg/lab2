package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    List<FridgeItem> items;

    public Fridge () {
        this.items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return this.items.size();
    }

    /**
     * The fridge has a fixed (final) max size.
     * Returns the total number of items there is space for in the fridge
     *
     * @return total size of the fridge
     */
    @Override
    public int totalSize() {
        return 20;
    }

    /**
     * Place a food item in the fridge. Items can only be placed in the fridge if
     * there is space
     *
     * @param item to be placed
     * @return true if the item was placed in the fridge, false if not
     */
    @Override
    public boolean placeIn(FridgeItem item) {
        // Step 1: Check if there is space in the fridge

        // Step 2: If there is space in the fridge, add the FridgeItem to the list

        // Step 3: Return true if an item was added to the list and return false if the fridge was full.

        if(this.totalSize() == this.nItemsInFridge()){
            return false;
        }
        this.items.add(item);
        return true;

    }

    /**
     * Remove item from fridge
     *
     * @param item to be removed
     * @throws NoSuchElementException if fridge does not contain <code>item</code>
     */
    @Override
    public void takeOut(FridgeItem item) throws NoSuchElementException {
        //chech if item excist in FridgeItem.
        //remove selected item from fridgeItem
        boolean itemRemoved = false;
        for(int i = 0; i < this.nItemsInFridge(); i++){
            if(this.items.get(i).equals(item)){
                items.remove(i);
                itemRemoved = true;
                break;
            }
        }
        if(!itemRemoved){
            throw new NoSuchElementException();
        }
    }

    /**
     * Remove all items from the fridge
     */
    @Override
    public void emptyFridge() {
        this.items.clear();

    }

    /**
     * Remove all items that have expired from the fridge
     * @return a list of all expired items
     */
    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        List<FridgeItem> tempList = new ArrayList<FridgeItem>(this.items);
        for(int i = 0; i < tempList.size(); i++) {
            // Bruk items.get(i).hasExpired for å sjekke om det har gått ut
            if (tempList.get(i).hasExpired()) {
                FridgeItem badItem = tempList.get(i);
                expiredItems.add(badItem);
                this.takeOut(badItem);
            }
        }
        return expiredItems;
    }



}
